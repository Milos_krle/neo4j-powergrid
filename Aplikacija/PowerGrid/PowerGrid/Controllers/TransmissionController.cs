﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using PowerGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Controllers
{
    public class TransmissionController : Controller
    {
        private readonly GraphClient client;
        public TransmissionController()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "neo4jj");

            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public IActionResult ShowTransmission()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Transmission) RETURN n ", new Dictionary<string, object>(), Neo4jClient.Cypher.CypherResultMode.Set);
            List<Transmission> trans = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query).ToList();
            return View(trans);
        }

        public IActionResult EditTransmission(int? id)
        {
            if (id == null) return RedirectToAction("ShowTransmission", "Transmission");
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Transmission) where n.ID_transm ={id} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            Transmission gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query).FirstOrDefault();
            return View(gen);
        }

        public IActionResult EditTransmissionData(Transmission t)
        {
            //editovanje
            string strq = $"match (n:Transsmison) where n.ID_transm = {t.ID_transm}" +
                $" set  n.LokX = {t.LokX}" +
                $", n.LokY ={t.LokY}" +
                $", n.NaponskiNivoKV ={t.NaponskiNivo}" +
                $", n.PresekProvodnika ={t.PresekProvodnikaMM2}" +
                $", n.KratakSpoj ={t.KratakSpoj}" +
                
                $" return n ;";
            var query = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
            Transmission gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query).Single();

           
            return RedirectToAction("ShowTransmission", "Transmission");
        }


        public IActionResult DeleteGenerator(int? id)
        {
            if (id == null) return RedirectToAction("ShowTransmission", "Transmission"); //vracam se na pocetnu
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("id", id);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Transmission) where n.ID_transm = {id} delete n ;", queryDict, CypherResultMode.Projection);
            Transmission gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query).FirstOrDefault();
            //gen ce biti null sto znaci da je obrisan
            return RedirectToAction("ShowTransmission", "Transmission");
        }

        //prikaz svih entiteta mora preko RWIRE jer ima liste elemenata 

        public IActionResult CreateTransmission()
        {
            Transmission tran = new Transmission();
            return View(tran);
        }

        public IActionResult CreateTransmissionData(Transmission t)
        {


            if (t == null) return RedirectToAction("ShowTransmission", "Transmission");


            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ID_transm", t.ID_transm);
            queryDict.Add("LokX", t.LokX);
            queryDict.Add("LokY", t.LokY);
            queryDict.Add("NaponskiNivoKV", t.NaponskiNivo);
            queryDict.Add("PresekProvodnika", t.PresekProvodnikaMM2);
            queryDict.Add("KratakSpoj", t.KratakSpoj);
 

            string q = "CREATE (n:Transmission {ID_transm:'" + t.ID_transm + "', LokX:'" + t.LokX
                                                            + "', LokY:'" + t.LokY + "', NaponskiNivoKV:'" + t.NaponskiNivo
                                                            + "', PresekProvodnika:'" + t.PresekProvodnikaMM2
                                                            + "', KratakSpoj:'" + t.KratakSpoj
                                                            + "'}) return n";

            var query = new Neo4jClient.Cypher.CypherQuery(q, queryDict, CypherResultMode.Set);
            List<Transmission> gener = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query).ToList();
            return RedirectToAction("ShowTransmission", "Transmission");
        }

        public IActionResult ShowNodeInDanger()
        {
            string strr = "match(n: Transmission) -[r: RWIRE]->(m: Distribution) where m.KratakSpoj = true return m;";
            var query = new Neo4jClient.Cypher.CypherQuery(strr, new Dictionary<string, object>(), CypherResultMode.Set);
            List<Distribution> gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).ToList();
            return View(gen);
        }

        public IActionResult FixDistribution(long id)
        {
            string strq = $" match (n:Distribution) where n.ID_dis ={id} set n.KratakSpoj = false return n;";
        
            var query1 = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
            Transmission gen1 = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query1).Single();
            return RedirectToAction("ShowTransmission", "Transmission");
        }
    }
}
