﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using PowerGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Controllers
{
    public class CustomerController : Controller
    {
        private readonly GraphClient client;
        public CustomerController()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "neo4jj");

            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public IActionResult ShowCustomer()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Customer) RETURN n ", new Dictionary<string, object>(), Neo4jClient.Cypher.CypherResultMode.Set);
            List<Customer> c = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).ToList();
            return View(c);
        }

        public IActionResult EditCustomer(int? id)
        {
            if (id == null) return RedirectToAction("ShowCustomer", "Customer");
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Customer) where n.ID_dis ={id} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            Customer gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).FirstOrDefault();
            return View(gen);
        }

        public IActionResult EditCustomerData(Customer c)
        {
            //edit
            if (c == null) return RedirectToAction("ShowCustomer", "Customer");
            string strq = $"match (n:Customer) where n.ID_Cust = {c.ID_Cust}" +
               $" set  n.Naziv = '{c.Naziv}'" +
               $", n.Tip ='{c.Tip}'" +
               $", n.IstalisanaSnagaKW ={c.IstalisanaSnagaKW}" +
               $", n.PoprecniPresek ={c.PresekProvodnika}" +
               $", n.MTK ={c.MTK}" +
               $", n.Suspendovano ='{c.Suspendovan}'" +
              
               $" return n ;";
            var query = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
            Customer gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).Single();
            return RedirectToAction("ShowCustomer", "Customer");
        }
        public IActionResult DeleteCustomer(int? id)
        {
            if (id == null) return RedirectToAction("ShowCustomer", "Customer"); //vracam se na pocetnu
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("id", id);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Customer) where n.ID_Cust = {id} delete n ;", queryDict, CypherResultMode.Projection);
            Customer gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).FirstOrDefault();
            //gen ce biti null sto znaci da je obrisan 
      
            return RedirectToAction("ShowCustomer", "Customer");
        }

        public IActionResult CreateCustomer()
        {
            Customer c = new Customer();
            return View(c);
        }

        public IActionResult CreateCustomerData(Customer c)
        {
            if (c == null) return RedirectToAction("ShowCustomer", "Customer");

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ID_Cust", c.ID_Cust);
            queryDict.Add("Naziv", c.Naziv);
            queryDict.Add("Tip", c.Tip);
            queryDict.Add("IstalisanaSnagaKW", c.IstalisanaSnagaKW);
            queryDict.Add("PoprecniPresek", c.PresekProvodnika);
            queryDict.Add("MTK",c.MTK);
            queryDict.Add("Suspendovano", c.Suspendovan);
          

            string q = "CREATE (n:Customer {ID_Cust:'" + c.ID_Cust + "', Naziv:'" + c.Naziv
                                                            + "', Tip:'" + c.Tip + "', IstalisanaSnagaKW:'" + c.IstalisanaSnagaKW
                                                            + "', PoprecniPresek:'" + c.PresekProvodnika
                                                            + "', MTK:'" + c.MTK+ "', Suspendovano:'" + c.Suspendovan
                                                            
                                                            + "'}) return n";

            var query = new Neo4jClient.Cypher.CypherQuery(q, queryDict, CypherResultMode.Set);
            List<Customer> gener = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).ToList();




            return RedirectToAction("ShowCustomer", "Customer"); 
        }


    }
}
