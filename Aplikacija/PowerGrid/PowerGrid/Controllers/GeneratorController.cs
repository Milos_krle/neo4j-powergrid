﻿using Microsoft.AspNetCore.Mvc;
using PowerGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Neo4jClient;
using Neo4jClient.Cypher;


namespace PowerGrid.Controllers
{
    public class GeneratorController : Controller
    {
        private  GraphClient client = null;
        static int idd = 24;
        public GeneratorController()
        {
             client = new GraphClient(new Uri("http://localhost:7474/db/data"),"neo4j", "neo4jj");
       
            try
            {
                if (!client.IsConnected) client.Connect();

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }

        }


        public IActionResult ShowGenerators()
        {
            var query = new CypherQuery("match (n:Generator) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Generator> generators = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).ToList();
            return View(generators);
            


        }

        public IActionResult EditGenerator(int? id)
        {
            if (id == null) return RedirectToAction("ShowGenerators", "Generator"); //vracam se na pocetnu

            //pronadji gen i prosledi match (n:Generator) where n.ID_gen =2  return n
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Generator) where n.ID_gen ={id} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).FirstOrDefault();
            return View(gen);

        }

        public IActionResult EditGeneratorData(Generator g)
        {
            if (g == null) return RedirectToAction("ShowGenerators", "Generator");
            //vraca isti koji je i pozvala i menjamo samo one delove koje treba
            //     Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).FirstOrDefault();

            /*match (n:Generator)  where n.ID_gen =1
            set n.BrojPotrosaca=0, n.KratakSpoj=false
            return n;  */

            //ovo mogu kao da obrisem cvor i da dodam drugi 
            //a mogu i preko set pri cemu cu morati sve one izmene da sacuvam i da dodam
            string strq = $"match (n:Generator) where n.ID_gen = {g.ID_gen}" +
                $" set  n.SnagaGenKW = {g.SnagaGenKW}" +
                $", n.PocetakRada ='{(g.PocetakRada.Year.ToString() +"/" + g.PocetakRada.Month.ToString() + "/" + g.PocetakRada.Day.ToString()).ToString()}'" +
                $", n.IzlaznaStrujaA ={g.IzlaznaStrujaA}" +
                $", n.BrojPotrosaca ={g.BrojPotrosaca}" +
                $", n.Stanje ={g.Stanje}" +
                $", n.Naziv ='{g.Naziv}'" +
                $", n.IzlazniNaponKV = {g.IzlazniNaponKV} " +
                $" return n ;";
                var query = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
             Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).Single();

            return RedirectToAction("ShowGenerators", "Generator");
        }



        public IActionResult DeleteGenerator(int? id)
        {
            if (id == null) return RedirectToAction("ShowGenerators", "Generator"); //vracam se na pocetnu
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("id", id);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Generator) where n.ID_gen = {id} delete n ;", queryDict, CypherResultMode.Projection);
            Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).FirstOrDefault();
            //gen ce biti null sto znaci da je obrisan 
            return RedirectToAction("ShowGenerators", "Generator");
        }

        [HttpGet]
        public IActionResult CreateGenerator()
        {
            Generator gen = new Generator();
            return View(gen);
        }
        [HttpPost]
        public IActionResult CreateGeneratorModel(Generator gen)
        {
            if (gen == null) return RedirectToAction("ShowGenerators", "Generator");

      

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ID_gen", gen.ID_gen);
            queryDict.Add("SnagaGenKW", gen.SnagaGenKW);
            queryDict.Add("PocetakRada", gen.PocetakRada.Date);
            queryDict.Add("IzlaznaStrujaA", gen.IzlaznaStrujaA);
            queryDict.Add("BrojPotrosaca", gen.BrojPotrosaca);
            queryDict.Add("Stanje", gen.Stanje);
            queryDict.Add("Naziv", gen.Naziv);
            queryDict.Add("IzlazniNaponKV", gen.IzlazniNaponKV);

            string q = "CREATE (n:Generator {ID_gen:'" + gen.ID_gen + "', SnagaGenKW:'" + gen.SnagaGenKW
                                                            + "', PocetakRada:'" + gen.PocetakRada + "', IzlaznaStrujaA:'" + gen.IzlaznaStrujaA
                                                            + "', BrojPotrosaca:'" + gen.BrojPotrosaca
                                                            + "', Stanje:'" + gen.Stanje + "', Naziv:'" + gen.Naziv
                                                            + "', IzlazniNaponKV:'" + gen.IzlazniNaponKV
                                                            + "'}) return n";

            var query = new Neo4jClient.Cypher.CypherQuery(q, queryDict, CypherResultMode.Set);
            List<Generator> gener = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).ToList();

            return RedirectToAction("ShowGenerators", "Generator");

        }


        public bool DaLiPostoji(long id)
        {
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Generator) where n.ID_gen ={id} return n", new Dictionary<string, object>(), CypherResultMode.Projection);
            Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).FirstOrDefault();
            if (gen == null) return false;
            return true;
        }

        //jedino generator se povezuje sa dalekovodom
        public IActionResult ConnectGen(long id)
        {
            int radious = 26;
            //postavi u dicttionary parametar radious 
            var query2 = new Neo4jClient.Cypher.CypherQuery($"match (n:Transmission) where n.LokX < {radious} AND n.LokY < {radious} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Transmission> t = ((IRawGraphClient)client).ExecuteGetCypherResults<Transmission>(query2).ToList();
            //MATCH (a:Transmission),(b:Generator) 
            // WHERE a.ID_transm = 0 AND b.ID_gen = 2
            //CREATE(a) -[r: RWIRE { ID: 20 Tezina: 4, Aktivna: true }]->(b)
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            // queryDict.Add("id", id);
            // queryDict.Add("id_tran", t.ElementAt(0).ID_transm);
            queryDict.Add("Tezina", 4);
            queryDict.Add("Aktivna", true);
            long id_tran = t.ElementAt(0).ID_transm;
            string strr = "match (a:Generator), (b:Transmission) where a.ID_gen =" + id + " AND " + "b.ID_transm = " + id_tran +
                
                "create (a) -[r: RWIRE { ID: " + (idd++) + ", Tezina = 4, " + " Aktivna = true }]->(b) return a"
                
                ;

           
            var query3 = new Neo4jClient.Cypher.CypherQuery(strr, queryDict, CypherResultMode.Set);
            var rez = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query3).Single();

            return RedirectToAction("ShowGenerators", "Generator");
        }

        public IActionResult DisconnectGen(long idGen, long idTrans)
        {
            //da obrise vezu
            //ne znm kako da obrisem vezu a da ne obrisem sve koji su vezani za njih, mozda preko id 

            return RedirectToAction("ShowGenerators", "Generator");
        }

        public IActionResult ChangeState(int id) //generator menja stanje ON | OFF
        {
            bool stanje;
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Generator) where n.ID_gen ={id} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            Generator gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).FirstOrDefault();

            stanje = !gen.Stanje;


            string strq = $"match (n:Generator) where n.ID_gen = {id}" +
             $" set  n.Stanje ={stanje}" +
             $" return n ;";
            var query1 = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
            Generator gen1 = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query1).Single();

            return RedirectToAction("ShowGenerators", "Generator");



        }

        public IActionResult GetConnectedGenerators()
        {
            var query = new Neo4jClient.Cypher.CypherQuery($" match (n:Generator)-[r:RWIRE]->(generator) return generator", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Generator> gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Generator>(query).ToList();
            return View(gen);
        }

      

       
    }
}
