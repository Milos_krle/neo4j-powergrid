﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using PowerGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Controllers
{
    public class DistributionController : Controller
    {
        private readonly GraphClient client;
        public DistributionController()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "neo4jj");

            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public IActionResult ShowDistribution()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Distribution) RETURN n ", new Dictionary<string, object>(), Neo4jClient.Cypher.CypherResultMode.Set);
            List<Distribution> dis = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).ToList();
            return View(dis);
        }

        public IActionResult EditDistribution(int? id)
        {
            if (id == null) return RedirectToAction("ShowDistribution", "Distribution");
            var query = new Neo4jClient.Cypher.CypherQuery($"match (n:Distribution) where n.ID_dis ={id} return n", new Dictionary<string, object>(), CypherResultMode.Set);
            Distribution gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).FirstOrDefault();
            return View(gen);
        }

        public IActionResult EditDistributionData(Distribution d)
        {
            if (d == null) return RedirectToAction("ShowDistribution", "Distribution");
            string strq = $"match (n:Distribution) where n.ID_dis = {d.ID_dis}" +
              $" set  n.LokX = {d.LokX}" +
              $", n.LokY = {d.LokY}" +
              $", n.NaponskiNivoV ={d.NaponskiNivoV}" +
              $", n.PresekProvodnika ={d.PresekProvodnika}" +
              $", n.KratakSpoj ={d.KratakSpoj}" +
              $", n.tezinaM ={d.tezinaM}" +
              $", n.Rasveta = {d.Rasveta} " +
              $" return n ;";
            var query = new Neo4jClient.Cypher.CypherQuery(strq, new Dictionary<string, object>(), CypherResultMode.Set);
            Distribution gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).Single();
            return RedirectToAction("ShowDistribution", "Distribution");
        }
        public IActionResult DeleteDistribution(int? id)
        {
            if (id == null) return RedirectToAction("ShowDistribution", "Distribution"); //vracam se na pocetnu
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("id", id);
            var query = new Neo4jClient.Cypher.CypherQuery("match (n:Distribution) where n.ID_dis = {id} delete n ;", queryDict, CypherResultMode.Projection);
            Distribution gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).FirstOrDefault();
            return RedirectToAction("ShowDistribution", "Distribution"); 
        }
        //u view bi bilo dobro da stavimo fju koja gen kratak spoj i tako nes ali poziva i drugi entitet tipa za transm


        public IActionResult CreateDistribution()
        {
            Distribution d = new Distribution();
            return View(d);
        }

        public IActionResult CreateDistributionData(Distribution d)
        {

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ID_dis", d.ID_dis);
            queryDict.Add("LokX", d.LokX);
            queryDict.Add("LokY", d.LokY);
            queryDict.Add("NaponskiNivoV", d.NaponskiNivoV);
            queryDict.Add("PresekProvodnika", d.PresekProvodnika);
            queryDict.Add("KratakSpoj", d.KratakSpoj);
            queryDict.Add("tezinaM", d.tezinaM);
            queryDict.Add("Rasveta", d.Rasveta);


            string q = "CREATE (n:Distribution {ID_dis:'" + d.ID_dis + "', LokX:'" + d.LokX
                                                            + "', LokY:'" + d.LokY + "', NaponskiNivoV:'" + d.NaponskiNivoV
                                                            + "', PresekProvodnika:'" + d.PresekProvodnika
                                                            + "', KratakSpoj:'" + d.KratakSpoj
                                                            + "', tezinaM:'" + d.tezinaM
                                                            + "', Rasveta:'" + d.Rasveta
                                                            + "'}) return n";

            var query = new Neo4jClient.Cypher.CypherQuery(q, queryDict, CypherResultMode.Set);
            List<Distribution> gener = ((IRawGraphClient)client).ExecuteGetCypherResults<Distribution>(query).ToList();
            return RedirectToAction("ShowDistribution", "Distribution");
        }
        public IActionResult ConnectedCustomers(long id) //id dis
        {
            string str = $" match (n:Distribution)-[r:RWIRE]->(m:Customer) where n.ID_dis = {id} and m.Suspendovano = false return  m;";
            var query = new Neo4jClient.Cypher.CypherQuery(str, new Dictionary<string, object>(), CypherResultMode.Set);
            List<Customer> gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).ToList();
            return View(gen);
        }

        public IActionResult BlockCustomer(long id)
        {
           
            var query = new Neo4jClient.Cypher.CypherQuery($" match(m:Customer) where m.ID_Cust = {id} set m.Suspendovano=true return  m;", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Customer> gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).ToList();
          
            return RedirectToAction("ShowDistribution", "Distribution");
        }

        public IActionResult UnblockCustomer(long id)
        {
            var query = new Neo4jClient.Cypher.CypherQuery($" match(m:Customer) where m.ID_Cust = {id} set m.Suspendovano=false return  m;", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Customer> gen = ((IRawGraphClient)client).ExecuteGetCypherResults<Customer>(query).ToList();

            return RedirectToAction("ShowDistribution", "Distribution");
        }

    }
}
