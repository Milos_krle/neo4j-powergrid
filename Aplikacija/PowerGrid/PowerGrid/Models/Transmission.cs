﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Models
{
    public class Transmission
    {
     //   public int Id { get; set; }
        public long ID_transm { get; set; }
        public double LokX { get; set; }
        public double LokY { get; set; }
        public float PresekProvodnikaMM2 {get; set;}
        public int NaponskiNivo { get; set; }
        public bool KratakSpoj { get; set; }


        public Transmission ()
        {
            LokX = LokY = 0.0;
        }

      
    }
}
