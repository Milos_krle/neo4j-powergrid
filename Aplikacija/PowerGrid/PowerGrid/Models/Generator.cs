﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Models
{
    public class Generator
    {
       // public int Id { get; set; }
        public long ID_gen { get; set; }
        public long SnagaGenKW { get; set; }
        public DateTime PocetakRada { get; set; }
        public long IzlaznaStrujaA { get; set; }
        public int BrojPotrosaca { get; set; }
        public bool Stanje { get; set; } //on | off

        public String Naziv { get; set; }
        public long IzlazniNaponKV { get; set; }

        public Generator()
        {
            Naziv = "";
        }
 
    }

  
}
