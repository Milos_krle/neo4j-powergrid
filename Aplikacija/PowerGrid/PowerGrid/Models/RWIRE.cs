﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Models
{
    public class RWIRE
    {
        public int Id { get; set; }
        public int Tezina { get; set; }
        public bool Aktivna { get; set; }
        public int BrZice { get; set; } //? mada moze i da se ne koristi


        //valjda je potrebno da bi se kreirale veze //nisam sig, ispravite me ako gresim ovde 
        public IList<Generator> Generators { get; set; }
        public IList<Transmission> Transmissions { get; set; }

        public IList<Distribution> Distributions { get; set; }

        public IList<Customer> Customers { get; set; }

        public RWIRE ()
        {
            Generators = new List<Generator>();
            Transmissions = new List<Transmission>();
            Distributions = new List<Distribution>();
            Customers = new List<Customer>();
        }

    }
}
