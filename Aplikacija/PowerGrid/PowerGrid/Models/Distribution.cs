﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Models
{
    public class Distribution
    {
      //  public int Id { get; set; }
        public long ID_dis { get; set; }
        public double LokX { get; set; }
        public double LokY { get; set; }
        public bool Rasveta { get; set; }
        public int NaponskiNivoV { get; set; }
        public int tezinaM { get; set; } //ukoliko se radi preko neki alg da imamo tezinu svakog potega
        public bool KratakSpoj { get; set; }
        public int PresekProvodnika { get; set; }
    }
}
