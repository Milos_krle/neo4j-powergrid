﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PowerGrid.Models
{
    public class Customer
    {
      //  public int Id { get; set; }
        public long ID_Cust { get; set; }
        public String Tip { get; set; }
        public int PresekProvodnika { get; set; } //kaze koji pop presek voda je do domacinstva iskoriscen
        public double IstalisanaSnagaKW { get; set; }
        public String Naziv { get; set; }
        public bool Suspendovan { get; set; }
        public bool MTK { get; set;  } //niza ili visa tarifa, udaljeno pracenje i td


        public Customer()
        {

        }
    }
}
