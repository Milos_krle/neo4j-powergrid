ZADATAK 1 (CASSANDRA) ReadGoods

	-Modeli podataka
		+Korisnik
		+Knjiga
		+Autor
		+Polica
	-CRUD operacije
		+Create knjiga/autor/korisnik/polica
		+Read knjiga/autor/korisnik/polica
		+Update knjiga/autor/korisnik/polica
		+Delete korisnik/polica
	-Funkcionalnosti
		+Create polica
		+Like knjiga
		+Recommended sistem
		+SearchByName
		+SearchByAuthor
		?SearchByGenre
		?CommentsOf knjiga
		?Comment knjiga
		
Cassandra - GreatReads
Web aplikacija za vođenje evidencije o knjigama i autorima, koje korisnici mogu da pregledavaju, lajkuju, da organizuje knjige na svojim policama, pretražuju
knjige i dobijaju preporuke za čitanje.
------------------------------------------------------------------

ZADATAK 2 (NEO4J) DatingApp

	-Modeli podataka
		+Korisnik
		+Like
		+Dislike
		+Block
		+Recommended
		?Friend
	-CRUD operacije
		+Create korisnik
		+Read korisnik
		+Update korisnik
		+Delete korisnik
	-Funkcionalnosti
		+Match (međusobno lajkovanje)
		+Recommend (na osnovu lajka, rastojanja, prijateljstva)
		+RecommendByFriend

NEO4J - Kresivo
Web aplikacija za upoznavanje poput Tindera koja daje korisnicima mogućnost lajkovanja drugih korisnika, preporučivanje veze prijateljima, 
upoznavanje drugih korisnika.

------------------------------------------------------------------
ZADATAK 2 (NEO4J) PowerGrid

	-Modeli podataka
		+GeneratorNode
		+TransmisionNode
		+DistributionNode
		+CustomerNode
	-CRUD operacije
		+Create NodeType
		+Read NodeType
		+Upadate NodeType
		+Delete NodeType
	-Funkcionalnosti
		+PowerGridCheck
		+DisableCustomer

transmission - dalekovod 
distribution - distributivni sistem

//konsultovao se s drugara i ovo su realni termini po ugledu na realni sistem MKT

1) GeneratorNode 
	-ID gen





	-Izlazni napon na generator  : racunamo u KV
	-izlazna struja 	u 
	-snaga generatora : INT 

	-naziv gen : String 
	
	-Stanje ( da li je ukljucen, ili ne ) 
	-DateTime za obracun rada PocetakRada
	-Dozvoljeni broj potrosaca

2) TransmissionNode  (dalekovod) -fabrike, industrije i td 
	-ID dalekovoda 
	-Lokacija (ako ne drugo x i y koordinate)
	-naponski nivo 
	-presek provodnika 
	? -visina 
             " "	-napon na pocetku i na kraju ( ukoliko su razliciti moze doci do pregorevanja uredjaja u domacinstvu koja su povezana )  (pocetak ==  kraju) znaci da nema problema gde na kraju mora biti malo manji od pocetka znaci da je dobro 
	  (u sup bilo koja varijacija je napravljen kursus )  
	-kratak spoj (T | F) //za testiranje funkcionanosti
	//-rastojanje izmedju bandere dalekovoda je fiksno i to je 150m 
	
CONSTANTA_RASTOJANJE za dalekovod 150, za dist 100

3) DistrubutionNode (lokalni vod) do obicnih potrosaca
	-naponski nivo napona  (ne utice mng efekat na pocetku i na kraju ali moze da nam sluzi za detekciju otkaza)
	-id 
	-presek prov -> zavisi od broja_potrosaca
	-ulicna rasveta 
   	-kratak spoj (T | F) //za testiranje funkcionanosti
	
	 broj provodnika (monofazni, trofazni i br zila za rasvetu)
	-naziv 
	-pozicija 
	-broj_potrosaca 
	-tezina(rastojanje)(promenljivo rastojanje 100m ) (koja omogucava da se pronadje optimalno rastojanje) za dodavanje domacinstva 



4) CustomerNode (domacinstvo)
	-id domacinstva 
	-naziv 
	-tip 
	-Istalisana snaga (snaga koja je potrebna domacinstvu) -koliko jedno domacinstvo moze da povuce snagu
	-presek provodnika 
	 
	-MTK da moze da vrsi kontrolu vise i nize tarife iz distribucije  (Node DistribucioniCentar) 
	
//ukoliko dodamo domacinstvo koje zahteva vecu znagu od dozvoljene na banderi, nije moguce povezati//
//sabrati ukupnu potrosnju domacinstva za celu mrezu i odrediti procenat iskoriscenosti 

-VEZEE 
________________________ 
-RWIRE
	-tezina
	-aktivna(T | F)
	-brZice






CRUD operacije za sva 4 entiteta 
 -prikaz 
-dodavanje 
-update 
-delete 

--FUNKCIONALNOSTI 

--Provera mreze (da li postoje konflikti ili ne ) 
--Blokiraj Domacinstvo | fabriku 
--Usled Konflikta naci optimalnu vezu i prespojiti
--DetekcijaGreske na Dalekovodu (Transmission)  | distributivnim(lokalnom) 
--Ukljuciti | iskljuciti dodatne generatore ukoliko je snaga na svim domacinstvima prelazi snagu generatora 
--Iskljuci sve generatore koje rade duze od INT vrednost h
//naredbe 

//kreiranje generatora
	CREATE (gen1:Generator { ID_gen: 1, IzlazniNaponKV: 250000, IzlaznaStrujaA: 25000, 
	SnagaGenKW: 6250000, Naziv: 'Gen1',  Stanje: true,PocetakRada: "2019-06-01T18:40:32.142+0100",BrojPotrosaca: 0 })

//kreiranje dalekovoda	
CREATE (dalek1:Transmission { ID_transm: 0, LokX: 24.123, LokY: 25.1245, NaponskiNivoKV: 180, PresekProvodnika: 64, KratakSpoj: false })

//kreiranje distributivne mreze
CREATE (dist1:Distribution { ID_dis: 0, LokX: 24.123, LokY: 25.1245, NaponskiNivoV: 230, PresekProvodnika: 10, KratakSpoj: false, tezinaM:100, Rasveta: true})

//distrtibutivni kupac -koji ima neku fabriku i nesto, ostali obicna dom spadaju pod opsta a uvodim jos jednu koja 
//istalisana snaga mora biti manja od 42KW za obicna domacinstva i oni se povezuju na dist mrezu
za vecu spadaju distrubutivna
	
//kreiranje domacinstva
CREATE (dom:Customer { ID_Cust: 0, Naziv: 'Domacinstvo1', Tip: 'Distributivna', IstalisanaSnagaKW: 80, 20, MTK: true, Suspendovano: false}) //kada kliknemo da blokiramo domacinstvo ovo se setuje na true i ne prikazuje se 

//brisanje veze 
Match ()-[r]-() Where ID(r)=1 Delete r

//kreiranje veze izmedju generatore
MATCH (a:Generator),(b:Generator) 
WHERE a.ID_gen = 1 AND b.ID_gen = 2
CREATE (a)-[r:RWIRE {Tezina: 4, Aktivna: true }]->(b)
//
MATCH (a:Transmission),(b:Generator) 
WHERE a.ID_transm = 0 AND b.ID_gen = 2
CREATE (a)-[r:RWIRE {Tezina: 4, Aktivna: true }]->(b) 

//za postavljanje veza izmedju dalekovoda 
MATCH (a:Transmission),(b:Generator) 
WHERE a.ID_transm = 0 AND b.ID_gen = 2
CREATE (a)-[r:RWIRE {Tezina: 4, Aktivna: true }]->(b) 

od dalekovod ka generatoru